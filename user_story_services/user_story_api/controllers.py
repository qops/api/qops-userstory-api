########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
    flash, g, session, redirect, url_for

import logging

# Import models
from .models import UserStory
from .models import UserStoryState
from ..mod_person.models import Person
from ..mod_backlog.models import Backlog

from .forms import UserStoryProfileForm

# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_user_story = Blueprint('user_story', __name__, url_prefix='/userstory')


@mod_user_story.context_processor
def store():
    store_dict = {'serviceName': 'User Story',
                  'serviceDashboardUrl': url_for('user_story.dashboard'),
                  'serviceBrowseUrl': url_for('user_story.browse'),
                  'serviceNewUrl': url_for('user_story.new'),
                  }
    return store_dict

# Set the route and accepted methods


@mod_user_story.route('/', methods=['GET'])
def user_story():
    return render_template('user_story/user_story_dashboard.html')


@mod_user_story.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('user_story/user_story_dashboard.html')


@mod_user_story.route('/browse', methods=['GET'])
def browse():
    user_stories = UserStory.query.with_entities(UserStory.id, UserStory.identifier, Backlog.name.label('backlog_id'), UserStory.name, UserStoryState.name.label('state_id'), Person.first_name.label(
        'person_id')).join(UserStoryState, UserStory.state_id == UserStoryState.id).join(Person, UserStory.author_id == Person.id).join(Backlog, UserStory.backlog_id == Backlog.id).all()
    logger = logging.getLogger('qops')
    logger.debug(len(user_stories))
    return render_template('user_story/user_story_browse.html', user_stories=user_stories)


@mod_user_story.route('/new', methods=['GET', 'POST'])
def new():
    user_story = UserStory()
    form = UserStoryProfileForm(request.form)
    form.state_id.choices = [(s.id, s.name)
                             for s in UserStoryState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]
    form.backlog_id.choices = [(b.id, b.name) for b in Backlog.query.all()]
    if request.method == 'POST':
        logger = logging.getLogger('qops')
        logger.debug('IN USER_STORY.NEW(POST)')
        logger.debug('UserStory Identifier : {0}'.format(form.identifier.data))
        logger.debug('UserStory Name       : {0}'.format(form.name.data))
        logger.debug('UserStory Description: {0}'.format(
            form.description.data))
        logger.debug('UserStory State      : {0}'.format(form.state_id.data))
        logger.debug('UserStory Author     : {0}'.format(form.author_id.data))
        logger.debug('UserStory Owner      : {0}'.format(form.owner_id.data))
        form.populate_obj(user_story)
        user_story.identifier = UserStory.get_next_identifier()
        db.session.add(user_story)
        db.session.commit()
        return redirect(url_for('user_story.browse'))
    return render_template('user_story/user_story_new.html', user_story=user_story, form=form)


@mod_user_story.route('/profile', methods=['GET', 'POST'])
@mod_user_story.route('/profile/<int:user_story_id>/profile', methods=['GET', 'POST'])
def profile(user_story_id=None):
    user_story = UserStory.query.get(user_story_id)
    form = UserStoryProfileForm(obj=user_story)
    form.state_id.choices = [(s.id, s.name)
                             for s in UserStoryState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]
    form.backlog_id.choices = [(b.id, b.name) for b in Backlog.query.all()]
    if request.method == 'POST':
        form = UserStoryProfileForm(request.form)
        form.populate_obj(user_story)
        db.session.add(user_story)
        db.session.commit()
        return redirect(url_for('user_story.browse'))
    return render_template('user_story/user_story_profile.html', user_story=user_story, form=form)


@mod_user_story.route('/view', methods=['GET', 'POST'])
@mod_user_story.route('/view/<int:user_story_id>/view', methods=[
    'GET', 'POST'])
def user_story_view(user_story_id=None):
    #user_story = UserStory.query.get(user_story_id)
    form = UserStoryProfileForm(obj=user_story)
    form.state_id.choices = [(s.id, s.name)
                             for s in UserStoryState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]
    form.backlog_id.choices = [(b.id, b.name) for b in Backlog.query.all()]
    if request.method == 'POST':
        form = UserStoryProfileForm(request.form)
        form.populate_obj(user_story)
        db.session.add(user_story)
        db.session.commit()
        return redirect(url_for('user_story.browse'))
    return render_template('user_story/user_story_view.html',
                           user_story=user_story, form=form)


@mod_user_story.route('/profile/dashboard', methods=['GET'])
@mod_user_story.route('/profile/<int:user_story_id>/dashboard', methods=['GET'])
def user_story_dashboard(user_story_id=None):
    if user_story_id:
        user_story = UserStory.query.get(user_story_id)
    else:
        user_story = None
    return render_template('user_story/user_story_single_dashboard.html', user_story=user_story)


@mod_user_story.route('/profile/tasks', methods=['GET'])
@mod_user_story.route('/profile/<int:user_story_id>/tasks', methods=['GET'])
def user_story_tasks(user_story_id=None):
    if user_story_id:
        user_story = UserStory.query.get(user_story_id)
    else:
        user_story = None
    return render_template('user_story/user_story_single_tasks.html', user_story=user_story)


@mod_user_story.route('/profile/communication', methods=['GET', 'POST'])
@mod_user_story.route('/profile/<int:user_story_id>/communication', methods=['GET'])
def user_story_communication(user_story_id=None):
    if user_story_id:
        user_story = UserStory.query.get(user_story_id)
    else:
        user_story = None
    return render_template('user_story/user_story_single_communication.html', user_story=user_story)


@mod_user_story.route('/profile/documents', methods=['GET', 'POST'])
@mod_user_story.route('/profile/<int:user_story_id>/documents', methods=['GET'])
def user_story_documents(user_story_id=None):
    if user_story_id:
        user_story = UserStory.query.get(user_story_id)
    else:
        user_story = None
    return render_template('user_story/user_story_single_documents.html', user_story=user_story)


@mod_user_story.route('/profile/builds', methods=['GET', 'POST'])
@mod_user_story.route('/profile/<int:user_story_id>/builds', methods=['GET'])
def user_story_builds(user_story_id=None):
    if user_story_id:
        user_story = UserStory.query.get(user_story_id)
    else:
        user_story = None
    return render_template('user_story/user_story_single_builds.html', user_story=user_story)


@mod_user_story.route('/profile/requirements', methods=['GET', 'POST'])
@mod_user_story.route('/profile/<int:user_story_id>/requirements', methods=['GET'])
def user_story_requirements(user_story_id=None):
    if user_story_id:
        user_story = UserStory.query.get(user_story_id)
    else:
        user_story = None
    return render_template('user_story/user_story_single_requirements.html', user_story=user_story)


@mod_user_story.route('/profile/customers', methods=['GET', 'POST'])
@mod_user_story.route('/profile/<int:user_story_id>/customers', methods=['GET'])
def user_story_customers(user_story_id=None):
    if user_story_id:
        user_story = UserStory.query.get(user_story_id)
    else:
        user_story = None
    return render_template('user_story/user_story_single_customers.html', user_story=user_story)


@mod_user_story.route('/profile/locations', methods=['GET', 'POST'])
@mod_user_story.route('/profile/<int:user_story_id>/builds', methods=['GET'])
def user_story_locations(user_story_id=None):
    if user_story_id:
        user_story = UserStory.query.get(user_story_id)
    else:
        user_story = None
    return render_template('user_story/user_story_single_locations.html', user_story=user_story)


@mod_user_story.route('/profile/issues', methods=['GET', 'POST'])
@mod_user_story.route('/profile/<int:user_story_id>/issues', methods=['GET'])
def user_story_issues(user_story_id=None):
    if user_story_id:
        user_story = UserStory.query.get(user_story_id)
    else:
        user_story = None
    return render_template('user_story/user_story_single_issues.html', user_story=user_story)


@mod_user_story.route('/profile/test-cases', methods=['GET', 'POST'])
@mod_user_story.route('/profile/<int:user_story_id>/test-cases', methods=['GET'])
def user_story_test_cases(user_story_id=None):
    if user_story_id:
        user_story = UserStory.query.get(user_story_id)
    else:
        user_story = None
    return render_template('user_story/user_story_single_test_cases.html', user_story=user_story)
