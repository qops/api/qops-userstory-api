########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref



from qops_desktop import db


class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key = True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


class UserStory(Base):
    __tablename__ = 'user_story'
    
    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    description = db.Column(db.String)
    state_id = db.Column(db.Integer, db.ForeignKey('user_story_state.id'))
    author_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    owner_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    backlog_id = db.Column(db.Integer, db.ForeignKey('backlog.id'))
    def get_next_identifier():
        return 'US00001'


class UserStoryVote(Base):
    __tablename__ = 'user_story_vote'

    user_story_id = db.Column(db.Integer, db.ForeignKey('user_story.id'))
    person_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    vote = db.Column(db.Integer)
    comment = db.Column(db.String)
    timestamp = db.Column(db.DateTime)


class UserStoryRequirements(Base):
    __tablename__ = 'user_story_requirements'
    
    user_story_id = db.Column(db.Integer)
    requirement_id = db.Column(db.Integer)


class UserStoryState(Base):
    __tablename__ = 'user_story_state'
    
    identifier = db.Column(db.String)
    name = db.Column(db.String)
    usage = db.Column(db.String)


class UserStoryChampions(Base):
    __tablename__ = 'user_story_champions'
    
    user_story_id = db.Column(db.Integer)
    person_id = db.Column(db.Integer)


class UserStoryStateHistory(Base):
    __tablename__ = 'user_story_state_history'
    
    user_story_id = db.Column(db.Integer)
    original_state_id = db.Column(db.Integer)
    new_state_id = db.Column(db.Integer)
    person_id = db.Column(db.Integer)


class UserStoryCommunication(Base):
    __tablename__ = 'user_story_communication'
    
    user_story_id = db.Column(db.Integer)
    communication_id = db.Column(db.Integer)


class UserStoryPriority(Base):
    __tablename__ = 'user_story_priority'

    identifier = db.Column(db.String)
    name = db.Column(db.String)
    usage = db.Column(db.String)
